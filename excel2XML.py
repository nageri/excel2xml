import openpyxl
import sys


class Excel2xml:
    global MC, MR, book, sheet, result  # Available throughout the lifetime of the program

    try:
        '''Remember to pass the path of the excel file you are converting as a commandline argument'''
        path = sys.argv[1]

        book = openpyxl.load_workbook(path)

        for i in range(len(book.sheetnames)):  # Looping through the sheets in a workbook
            try:

                sheet = book.get_sheet_by_name(book.sheetnames[i])
                MR = sheet.max_row    # The number of rows in the sheet
                MC = sheet.max_column   # The number of columns in the sheet

            except Exception as e:
                print('First Block', type(e))
                print('First Block : {0}'.format(e))

            tags = []  # The tags for the sheets

            for c in range(sheet.max_column):  # Looping through the sheet to get the tags(title for each column
                tags.append(sheet.cell(row=2, column=c+1).value)
            '''The column arg is c+1, since column values must be at least 1, else 'ValueError' '''
            '''
            print("The tags are : ", tags)   # This line prints the tags that are in the file
            print(book.get_sheet_by_name(book.sheetnames[i]))
            print(MR)  # Prints the number of rows in the sheet
            print(MC)  # Prints the number of columns in the sheet
            '''
            MC = sheet.max_column
            ''' Just incase MC variable isn't reachable (But can be removed)'''
            '''  print("The number of columns is ", len(tags)) # Prints the number of tags '''

            try:
                # This is going to come out as a string, which will write to a file in the end.
                result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<report>\n"

                for r in range(3, MR):
                    result += "  <transaction>\n"
                    for c in range(len(tags)):

                        tag = tags[c].replace("#", "No.").upper() or tags[c].replace(' ', '_') or tags[c]
                        val = sheet.cell(row=r, column=c+1).value
                        if val is None:
                            continue

                        result += "    <%s>%s</%s>\n" % (tag, val, tag)
                    result += "  </transaction>\n"
                    print()
                # Close our pseudo-XML string.
                result += "</report>"
                # Write the result string to a file using the standard I/O.
                file_name = "XML_FILE %d.xml" % i
                f = open(file_name, "w")
                f.write(result)
                f.close()
                print("Files created successfully")
            except Exception as e:
                print('Block 2',type(e))
                print('Block 2 : {0}'.format(e))

    except Exception as e:
        print('General ', type(e))
        print('General: {0}'.format(e))
